﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
namespace MainProject
{

    public static partial class FrameworkExtension
    {

        public static void SetAlpha(this SpriteRenderer r, float alpha)
        {
            var c = r.color;
            r.color = new Color(c.g, c.g, c.b, alpha);
        }
        public static void SetAlpha(this Image img, float alpha)
        {
            var c = img.color;
            img.color = new Color(c.g, c.g, c.b, alpha);
        }

        public static Transform FindChildTag(this Transform obj, String tag)
        {
            for (int i = 0; i < obj.childCount; i++)
            {
                if (obj.GetChild(i).tag == tag) return obj.GetChild(i); ;
            }
            return null;
        }

        public static Image FindImage(this Transform obj, String name)
        {
            for (int i = 0; i < obj.childCount; i++)
            {
                if (obj.GetChild(i).name == name && obj.GetChild(i).GetComponent<Image>())
                    return obj.GetChild(i).GetComponent<Image>();
            }
            return null;
        }

        public static T Random<T> (this List<T> arr)
        {
            var val = arr[UnityEngine.Random.Range(0, arr.Count)];
            return val;
        }
    }
}
