﻿using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEditor.SceneManagement;

public static class ScriptableObjectUtility
{


    public static ScriptableObject CreateAsset<T>(string name) where T : ScriptableObject
    {
        string path = "Assets/Mik/Prefabs/Sets";
        string prefix = "";

        T asset = ScriptableObject.CreateInstance<T>();

        var SO_Card = asset as SO_Card;
        if (SO_Card) { path = "Assets/Mik/Prefabs/Sets/Cards"; prefix = "Card_I_"; }

        var SO_CardPers = asset as SO_CardPers;
        if (SO_CardPers) { path = "Assets/Mik/Prefabs/Sets/Pers"; prefix = "Card_P_"; }

        var SO_CardQuest = asset as SO_CardQuest;
        if (SO_CardQuest) { path = "Assets/Mik/Prefabs/Sets/Cards"; prefix = "Card_Q_"; }

        var SO_Effect = asset as SO_Effect;
        if (SO_Effect) { path = "Assets/Mik/Prefabs/Sets/Effects"; prefix = "Effect_"; }

        /*
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }
        */

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + prefix + name + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        // EditorUtility.SetDirty(asset);
        //  AssetDatabase.SaveAssets();
        // AssetDatabase.Refresh();
        // EditorUtility.FocusProjectWindow();
        // Selection.activeObject = asset;

        return asset as ScriptableObject;
    }
}