﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Extension
{

    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;
        public static bool IsApplicationsQuitting;
        private static System.Object _lock = new System.Object();
        public static T Instance
        {
            get
            {
                if (IsApplicationsQuitting) return null;

                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = FindObjectOfType<T>();

                        if (_instance == null)
                        {
                            var singleton = new GameObject("[SINGLETON] " + typeof(T));
                            singleton.AddComponent<T>();
                            DontDestroyOnLoad(singleton);
                        }
                    }
                    return _instance;
                }
            }
        }


        public virtual void OnDestroy()
        {
            IsApplicationsQuitting = true;
        }
    }
}
