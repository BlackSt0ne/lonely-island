﻿using UnityEngine;
using System.Collections;

public class AudioPlauer : MonoBehaviour {

	public AudioClip[] AudioList;
	public AudioSource SoundPlay;
    public float volume = 0.5f;

    // Use this for initialization
    void Start () 
	{
		SoundPlay.clip = AudioList[Random.Range(0,AudioList.Length)];
		SoundPlay.Play ();

	}
	
	// Update is called once per frame
	void Update () 
	{

		if (Input.GetKeyDown (KeyCode.Alpha0))
		{
			SoundPlay.volume = 0;
		}
		if (Input.GetKeyDown (KeyCode.Alpha9))
		{
			SoundPlay.volume = volume;
		}

		if (Input.GetKeyDown (KeyCode.Backspace))
		{
			SoundPlay.time = 0;
		}


		if (SoundPlay.time == 0)
		{
			SoundPlay.clip = AudioList[Random.Range(0,AudioList.Length)];
			SoundPlay.Play ();
		}

	}
}
