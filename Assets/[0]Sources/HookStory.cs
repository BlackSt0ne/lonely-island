﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MainProject
{
    public class HookStory : MonoBehaviour
    {

        // [HideInInspector]
        public Transform Story;
        public Transform CharSlot;
        public List<AudioClip> clipsOpen;
        StoryCard storyCard;



        private void Start()
        {
            if (Story)
            {
             
                Story.GetComponent<StoryCard>().Card = transform;
                storyCard = Story.GetComponent<StoryCard>();
                gameObject.name = Story.name;
            }
        }
        private void UpdateHookStory()
        {
            if (Story)
            {
                Story.GetComponent<StoryCard>().Card = transform;
                storyCard = Story.GetComponent<StoryCard>();
                gameObject.name = Story.name;
            }
        }


        public void OpenCard() { StartCoroutine(storyCard.OpenCard()); ManagerAudio.Instance.Play(clipsOpen.Random(), 0.3f); }

        public void OpenNextCard()
        {
            Scene.Instance.StoryCard.GetChild(Scene.Instance.StoryCard.childCount - 1).SetParent(Scene.Instance.Canvas);
            Scene.Instance.StoryCard.GetChild(Scene.Instance.StoryCard.childCount - 1).GetComponent<HookStory>().OpenCard();
        }

        public void SelectLeft() { storyCard.SelectLeft(); }

        public void SelectRight() { storyCard.SelectRight(); }

        public void ContinueStory(Transform NewStory)
        {
            //убераем старую карту
            Scene.Instance.StoryCard.GetChild(Scene.Instance.StoryCard.childCount - 1).SetParent(Scene.Instance.Canvas);
            //Добовляем новую
            Transform _newCard = Instantiate(transform, Scene.Instance.StoryCard);
            Transform _newstory = Instantiate(NewStory, _newCard);

            _newstory.GetComponent<StoryCard>().Character = Story.GetComponent<StoryCard>().Character;

            _newCard.rotation = Quaternion.Euler(0, 180, 0);
            _newCard.localPosition = Vector3.zero;
            _newCard.GetComponent<DragCard>().enabled = false;
            _newCard.GetComponent<DragCard>().TextLeft.GetComponent<CanvasGroup>().alpha = 0;
            _newCard.GetComponent<DragCard>().TextRight.GetComponent<CanvasGroup>().alpha = 0;
            _newCard.GetComponent<HookStory>().Story = _newstory;
            _newCard.GetComponent<HookStory>().UpdateHookStory();
            _newCard.GetComponent<HookStory>().OpenCard();

        }

    }
}
