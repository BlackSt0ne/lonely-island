﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


//Выбор ответа при спайпе карты влево или вправо 
namespace MainProject
{
    public class DragCard : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {

        Camera MainCamera;

        public Text TextLeft, TextRight;
 
        public List<AudioClip>  clipsDrag;

        public float SoundValue;

        float SpeedMove = 3f, SpeedAlpha = 0.05f;
        bool Move, SelectLeft;
        Vector2 offset, StartPos;

        int i = 0;

        private void Awake()
        {
            MainCamera = Camera.main;
            SoundValue = 0.3f;

        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            offset = transform.localPosition - MainCamera.ScreenToWorldPoint(eventData.position);
            StartPos = transform.position;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!Move)
            {
                Vector2 pos = MainCamera.ScreenToWorldPoint(eventData.position);
                transform.localPosition = pos + offset;

                transform.rotation = Quaternion.Euler(0, 0, -0.1f * transform.localPosition.x);
            }

        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Move = true;
            //Открытие следующей карты с историей

            if (!SelectLeft) GetComponent<HookStory>().SelectLeft();
            else GetComponent<HookStory>().SelectRight();
        
        }


        private void Update()
        {
            //Движение карты после выбора ответа
            if (Move)
            {
                if (transform.position.x > 0)
                {
                    transform.Translate(transform.position.x * SpeedMove * Time.deltaTime, 0, 0);
                    transform.Rotate(0, 0, -1f);
                }

                else { transform.Translate(-transform.position.x * -SpeedMove * Time.deltaTime, 0, 0); transform.Rotate(0, 0, 1f); }
                //Исчезновение карты после выбора ответа
                if (GetComponent<CanvasGroup>())
                    GetComponent<CanvasGroup>().alpha -= SpeedAlpha;

            }
            //Отображение текста слева или право относительно карты
            //ответ с права
            if (transform.localPosition.x > 0)
            {

                if (TextLeft)
                    TextLeft.GetComponent<CanvasGroup>().alpha += SpeedAlpha;
                TextRight.GetComponent<CanvasGroup>().alpha -= SpeedAlpha;

                if (SelectLeft || i == 0) { ManagerAudio.Instance.Play(clipsDrag.Random(), SoundValue); i++; }

                SelectLeft = false;
              
            }
            //ответ с лева
            if (transform.localPosition.x < 0)
            {

                if (TextRight)
                    TextRight.GetComponent<CanvasGroup>().alpha += SpeedAlpha;
                TextLeft.GetComponent<CanvasGroup>().alpha -= SpeedAlpha;
            
                if (!SelectLeft) ManagerAudio.Instance.Play(clipsDrag.Random(), SoundValue);

                SelectLeft = true;

            }
            //удаление карты
            if (Vector2.Distance(transform.position, StartPos) > 30)
            {
                //   GameObject.Destroy(gameObject);
            }
        }
    }
}


