﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace ObjectClasses
{

    public enum TypeItem : int { food, water, weapons, books }

    public class StoryPattern
    {
    }

    [System.Serializable]
    public class ItemData
    {
        public Transform Item;
        public string Name;
        public List<AudioClip>  clip;
    }
    [System.Serializable]
    public class ItemPattern
    {
        public string NameCategory;
        public ItemData[] itemdata;

    }
}
