﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ObjectClasses
{
    public enum Type { military, archeologist, doctor, thief }
    public enum eToolBar  { intellect, hunger, drought, health}

    [System.Serializable]
    public class CharPattern
    {
      
        public string Name;
        public Sprite[] Portrait;
        public Type type;

    }

    public class NameChar
    {
        public static string[] NameMale = { "Малколм", "Эрон", "Роналд", "Джон", "Чарлз", "Сесил", "Энтони", "Кристофер", "Джозеф", "Гарри" };
        public static string[] NameFemale = { "Бернис", "Джоан", "Вивиан", "Сьюзан", "Мэрилинн", "Беверли", "Мартина", "Клер", "Имоджен", "Эмис" };
    }

    public class NewChar
    {
        public string name;
        public Sprite Portrait;
        public Type type;
        public Transform Char;
    }

    [System.Serializable]
    public class ToolBar
    {
        public string name;

        public Slider S_intellect;
        public Slider S_hunger;
        public Slider S_drought;

        public SpriteRenderer SR_intellect;
        public SpriteRenderer SR_hunger;
        public SpriteRenderer SR_drought;

        public bool B_intellect;
        public bool B_hunger;
        public bool B_drought;
        public bool B_HP;

        public Transform T_intellect;
        public Transform T_hunger;
        public Transform T_drought;

    }

}
