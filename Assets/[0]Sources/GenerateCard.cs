﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MainProject
{
    public class GenerateCard : MonoBehaviour
    {
        public Transform[] Prehistory, CharStory, Story;
        public Transform MainCard, CaseStoryCard;
        public int ValueStoryCard;

        public bool _Prehistory;
        public bool _CharStory;
        public bool _Story;

        private Transform newCard;


        void Start()
        {

            StartCoroutine(CreateStoryCard());

        }
        //генерация карт
        IEnumerator CreateStoryCard()
        {
            //сюжетные карты
            if (_Story)
            {
                for (int i = 0; i < ValueStoryCard; i++)
                {
                    //создание карты
                    newCard = Instantiate(MainCard, CaseStoryCard);
                    //создание сюжета
                    newCard.GetComponent<HookStory>().Story = Instantiate(Story[Random.Range(0, Story.Length)], newCard);


                    yield return new WaitForSeconds(0.1f);
                }
            }

            //персонажи 
            if (_CharStory)
            {
                for (int i = 0; i < 4; i++)
                {
                    //создание карты
                    //int id = Random.Range(0, 4);
                    newCard = Instantiate(MainCard, CaseStoryCard);
                    //генерация персонажа
                    newCard.GetComponent<CharacterCard>().Create();
                    //создание сюжета
                    newCard.GetComponent<HookStory>().Story = Instantiate(CharStory[Random.Range(0, Story.Length - 1)], newCard);


                    yield return new WaitForSeconds(0.1f);
                }
            }
            if (_Prehistory)
            {
                //предыстория 
                for (int i = 0; i < Prehistory.Length; i++)
                {
                    //создание карты
                    newCard = Instantiate(MainCard, CaseStoryCard);
                    //создание сюжета
                    newCard.GetComponent<HookStory>().Story = Instantiate(Prehistory[i], newCard);

                    yield return new WaitForSeconds(0.1f);
                }
            }

            newCard.tag = "StartCard";
        }
    }
}
