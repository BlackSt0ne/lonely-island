﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectClasses;


namespace MainProject
{

    public class CharacterSteps : MonoBehaviour
    {

        public List<Transform> Char = new List<Transform>();
        int step = 0;
        float speed = 0.08f;

        public NewChar newChar
        {
            get { return NextCaracter(); }
            set
            {
                Char.Add(value.Char);
                value.Char.GetComponent<CharacterPanel>().UpdateData(value);
            }
        }

        public NewChar NextCaracter()
        {
            //новый персонаж
            Transform newChar = Char[step].GetChild(0);
            MoveCard moveCard = newChar.GetComponent<MoveCard>();

            moveCard.NewPosition(new Vector2(0, 12), speed);



            //Старый персонаж персонаж
            if (step != 0)
            {
                Transform oldChar = Char[step - 1].GetChild(0);
                oldChar.GetComponent<MoveCard>().NewPosition(Vector2.zero, speed);

            }
            else
            {
                Transform oldChar = Char[Char.Count - 1].GetChild(0);
                oldChar.GetComponent<MoveCard>().NewPosition(Vector2.zero, speed);

            }
            int i = step;
            if (step != Char.Count - 1)
                step++;
            else step = 0;
            StartCoroutine(UpdateToolBar());
            return Char[i].GetComponent<CharacterPanel>().newChar;

        }
        //потеря энергии (голод и жажда)
        public IEnumerator UpdateToolBar()
        {
            for (int i = 0; i < Char.Count; i++)
            {
                Char[i].GetComponent<CharacterPanel>()._droughtEnergy = -0.05;
                Char[i].GetComponent<CharacterPanel>()._hungerEnergy = -0.05;
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
