﻿using Extension;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MainProject
{

    public class Scene : Singleton<Scene>
    {
        public Transform Canvas;
        public Inventory Inventory;
        public Transform StoryCard;
        public Text StoryText;
        public Transform CharacterPanel;
        public CharacterSteps CharacterStep;

    }
}
