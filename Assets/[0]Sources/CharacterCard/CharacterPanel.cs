﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ObjectClasses;

namespace MainProject
{

    public class CharacterPanel : MonoBehaviour
    {

        // Дочерний объект, с портретом персонажа
        public Image Portrait;
        public Text Name;
        public ToolBar toolBar;

        public Color MainColor;
        public Color DownColor;
        public Color UPColor;

        public Transform WeponBox;
        public Transform ItemBox;
        public Transform ItemSlot;
        public Transform DestroyBox;
        private Transform NewWepon;
        private Transform NewItem;
        private Transform NewParent;

        double _NewHungerEnergy;
        double _NewDroughtEnergy;
        double _NewIntellectEnergy;
        double _NewHPEnergy;

        public double _hungerEnergy
        {
            set
            {
                _NewHungerEnergy -= System.Math.Round(value, 2); if (_NewHungerEnergy < 0) _NewHungerEnergy = 0; toolBar.B_hunger = true;
                NewParent = toolBar.T_hunger;
            }
        }
        public double _droughtEnergy
        {
            set
            {
                _NewDroughtEnergy -= System.Math.Round(value, 2); if (_NewDroughtEnergy < 0) _NewDroughtEnergy = 0; toolBar.B_drought = true;
                NewParent = toolBar.T_drought;
            }


        }
        public double _intellectEnergy
        {
            set
            {
                _NewIntellectEnergy -= System.Math.Round(value, 2); if (_NewIntellectEnergy < 0) _NewIntellectEnergy = 0; toolBar.B_intellect = true;
                NewParent = toolBar.T_intellect;
            }
        }
        public double _HPEnergy
        {
            set
            {
                _NewHPEnergy -= System.Math.Round(value, 2); if (_NewHPEnergy < 0) _NewHPEnergy = 0; toolBar.B_HP = true;
                NewParent = ItemBox;
            }
        }
        public Sprite _Wepon
        {
            set { SwapWepon(value); }
        }
        public Sprite _ItemUse
        {
            set { UseItem(value); }
        }
        public NewChar newChar;
        float speed = 0.002f;
        //Обновление внешнего вида персонажа
        public void UpdateData(NewChar newChar)
        {
            this.newChar = newChar;
            Name.text = newChar.name;
            Portrait.sprite = newChar.Portrait;
        }

        private void FixedUpdate()
        {
            //голод
            if (toolBar.B_hunger) toolBar.B_hunger = MoveToolBar(toolBar.S_hunger, _NewHungerEnergy, toolBar.SR_hunger);
            //жажда
            if (toolBar.B_drought) toolBar.B_drought = MoveToolBar(toolBar.S_drought, _NewDroughtEnergy, toolBar.SR_drought);
            //разум
            if (toolBar.B_intellect) toolBar.B_intellect = MoveToolBar(toolBar.S_intellect, _NewIntellectEnergy, toolBar.SR_intellect);
        }


        bool MoveToolBar(Slider Bar, double NewEnergy, SpriteRenderer UI)
        {
            if (System.Math.Round(Bar.value, 2) != NewEnergy)
            {

                if (NewEnergy > Bar.value)
                {
                    Bar.value += speed;
                    UI.color = DownColor;
                    if (Bar.value > NewEnergy) { UI.color = MainColor; return false; }
                }
                else
                {
                    Bar.value -= speed;
                    UI.color = UPColor;
                    if (Bar.value < NewEnergy) { UI.color = MainColor; return false; }
                }
                return true;
            }
            else
            {
                UI.color = MainColor;
                return false;
            }
        }

        void SwapWepon(Sprite img)
        {
            if (NewWepon != null)
            {
                NewWepon.SetParent(DestroyBox);
                NewWepon.GetComponent<MoveCard>().SpeedAlpha = 0.01f;
                NewWepon.GetComponent<MoveCard>().SpeedMove = 0.005f;
                NewWepon.GetComponent<MoveCard>().Visable = true;
                NewWepon.GetComponent<MoveCard>().enabled = true;
            }

            NewWepon = Instantiate(ItemSlot, WeponBox);
            NewWepon.GetComponent<Image>().sprite = img;
            NewWepon.GetComponent<MoveCard>().SpeedAlpha = 0.05f;
            NewWepon.GetComponent<MoveCard>().Visable = false;
            NewWepon.GetComponent<MoveCard>().enabled = true;
            NewParent = null;
        }
        void UseItem(Sprite img)
        {

            NewItem = Instantiate(ItemSlot, NewParent);

            Vector2 Newpos;
            if (NewParent == ItemBox)
            {
                Newpos = Vector2.zero;
                NewItem.GetComponent<MoveCard>().SpeedMove = 0.08f;
            }
            else Newpos = new Vector2(0, 20);
        
            NewItem.GetComponent<Image>().sprite = img;
            NewItem.GetComponent<CanvasGroup>().alpha = 1; 
            NewItem.GetComponent<MoveCard>().NewPos = Newpos;
            NewItem.GetComponent<MoveCard>().SpeedAlpha = 0.01f;
            NewItem.GetComponent<MoveCard>().Visable = true;
            NewItem.GetComponent<MoveCard>().enabled = true;
            NewParent = null;
        }

    }
}
