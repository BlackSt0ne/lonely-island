﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ObjectClasses;

namespace MainProject
{

    public class CharacterCard : MonoBehaviour
    {

        public CharPattern[] charPattern;

        public GameObject FrontCard;

        [HideInInspector]
        public CharPattern CharType;

        public NewChar newChar = new NewChar();

        public void Create()
        {
            //Тип персонажа
            CharType = charPattern[Random.Range(0, charPattern.Length)];
            //портрет персонажа
            newChar.Portrait = CharType.Portrait[Random.Range(0, CharType.Portrait.Length)];
            //имя персоажа
            newChar.name = NameChar.NameMale[Random.Range(0, NameChar.NameMale.Length)];
            //тип персоажа
            newChar.type = CharType.type;

            UpdateCard();
        }

        void UpdateCard()
        {
            FrontCard.GetComponent<Image>().sprite = newChar.Portrait;
        }
    }
}
