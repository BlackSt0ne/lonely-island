﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectClasses;

namespace MainProject
{
    public class Items : StoryCard
    {

        public ItemPattern[] ItemUP;
        private ItemData NewItem;

        public new void Start()
        {
            ItemPattern ip = ItemUP[Random.Range(0, ItemUP.Length)];
            NewItem = ip.itemdata[Random.Range(0, ip.itemdata.Length)];


            backgraund(NewItem.Item.GetComponent<StoryCard>().img);
        }

        public override void NewStory()
        {
            StartCoroutine(PortraitAlpha());
              
            Story(Character.name + " гуляя по острову находит " + NewItem.Name);
            TextLeft("Подойти ближе");
            TextRight("Пройти мимо");

            ManagerAudio.Instance.Play(NewItem.clip.Random(), 0.3f);
        }

        public override void SelectLeft()
        {
            Card.GetComponent<HookStory>().ContinueStory(NewItem.Item);
        }
        public override void SelectRight()
        {
            base.SelectLeft();
        }
    }
}
