﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainProject
{

    public class medicine : StoryCard
    {

        public List<AudioClip> clipUse;

        public override void NewStory()
        {
            Story("Вы делаете шаг ближе, предмет находится перед вами");
            TextLeft("Использовать");
            TextRight("Положить в инвентарь");
        }
        public override void SelectLeft()
        {
            ManagerAudio.Instance.Play(clipUse.Random(),0.3f) ;
            Character.Char.GetComponent<CharacterPanel>()._HPEnergy = 0.5;
            Character.Char.GetComponent<CharacterPanel>()._ItemUse = img;
            base.SelectLeft();
        }
        public override void SelectRight()
        {
            Scene.Instance.Inventory.Add(img);
            base.SelectRight();
        }
    }
}
