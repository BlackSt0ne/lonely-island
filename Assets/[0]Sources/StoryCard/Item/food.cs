﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace MainProject
{
    public class food : StoryCard
    {

        public float energy;
        public List<AudioClip>  clip;

        public override void NewStory()
        {

            Story(Character.name + " делает шаг ближе, предмет находится перед вами");
            TextLeft("Сесть");
            TextRight("Положить в инвентарь");

     

        }
        public override void SelectLeft()
        {
            ManagerAudio.Instance.Play(clip.Random(), 1f);
            Character.Char.GetComponent<CharacterPanel>()._hungerEnergy = energy;
            Character.Char.GetComponent<CharacterPanel>()._ItemUse = img;
            base.SelectLeft();
        }
        public override void SelectRight()
        { 
        
            Scene.Instance.Inventory.Add(img);
            base.SelectRight();
        }
    }
}
