﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainProject
{

    public class books : StoryCard
    {

        public override void NewStory()
        {
            Story("Вы делаете шаг ближе, предмет находится перед вами");
            TextLeft("Взять");
            TextRight("Положить в инвентарь");
        }

        public override void SelectLeft()
        {
            Character.Char.GetComponent<CharacterPanel>()._intellectEnergy = 1;
            Character.Char.GetComponent<CharacterPanel>()._ItemUse = img;
            base.SelectLeft();
        }

        public override void SelectRight()
        {
            Scene.Instance.Inventory.Add(img);
            base.SelectRight();
        }
    }
}
