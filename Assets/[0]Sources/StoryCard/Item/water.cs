﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainProject
{

    public class water : StoryCard
    {

        public float energy;

        public List<AudioClip> ClipsUse;

        public override void NewStory()
        {
            Story("Вы делаете шаг ближе, предмет находится перед вами");
            TextLeft("Выпить");
            TextRight("Положить в инвентарь");
        }

        public override void SelectLeft()
        {
            ManagerAudio.Instance.Play(ClipsUse.Random(), 0.4f);
            Character.Char.GetComponent<CharacterPanel>()._droughtEnergy = energy;
            Character.Char.GetComponent<CharacterPanel>()._ItemUse = img;
            Card.GetComponent<HookStory>().OpenNextCard();
        }

        public override void SelectRight()
        {
            Scene.Instance.Inventory.Add(img);
            base.SelectRight();
        }
    }
}
