﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainProject
{

    public class wepon : StoryCard
    {
      public List<AudioClip> clipUse;

        public override void NewStory()
        {
            Story("Вы делаете шаг ближе, предмет находится перед вами");
            TextLeft("Экипировать");
            TextRight("Положить в инвентарь");
        }

        public override void SelectLeft()
        {
            ManagerAudio.Instance.Play(clipUse.Random(), 0.3f);
            Character.Char.GetComponent<CharacterPanel>()._Wepon = (img);
            base.SelectLeft();         
        }

        public override void SelectRight()
        {
       
            Scene.Instance.Inventory.Add(img);
            base.SelectRight();
        }
    }
}
