﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectClasses;

namespace MainProject
{

    public class CharStory : StoryCard
    {
        List<string> charStory = new List<string>();

        public Transform CharPanel;

        NewChar newChar;


		void Start0()
        {
            newChar = Card.GetComponent<CharacterCard>().newChar;
            charStory.Add(newChar.name + "                                   Военный - в отставке, хорошо разбирается с любым типом оружия");
            charStory.Add(newChar.name + "<b></b>                                   Археолог - любит возиться в грязи и разбираться со всякими странными и загадочными штуками");
            charStory.Add(newChar.name + "<b></b>                                   Врач - с большим опытом этому человеку можно доверь свою жизнь");
            charStory.Add(newChar.name + "<b></b>                                   Вор - который пытается забыть о своем непростом прошлым, но похоже все придется вспомнить свои навыки ");

        }

        public override void NewStory()
        {
            Story(charStory[(int)newChar.type]);
            TextLeft("Класс!");
            TextRight("Супер!");
        }

        public override void SelectLeft()
        {
         
            Transform Char = Instantiate(CharPanel, Scene.Instance.CharacterPanel);
            newChar.Char = Char;
            Scene.Instance.CharacterStep.newChar = newChar;

            base.SelectLeft();
        }
        public override void SelectRight()
        {
            SelectLeft();
        }

    }
}
