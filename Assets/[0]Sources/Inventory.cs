﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MainProject
{

    public class Inventory : MonoBehaviour
    {

        public List<AudioClip> ClipsIventory;

        public void Add(Sprite _img)
        {

            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).name == "slot" && !transform.GetChild(i).GetComponent<Image>().sprite)
                {
                    ManagerAudio.Instance.Play(ClipsIventory.Random(), 0.4f);
                    transform.GetChild(i).GetComponent<Image>().sprite = _img;
                    transform.GetChild(i).GetComponent<Image>().SetAlpha(1);
                    break;
                }
            }
            
        }
    }
}
