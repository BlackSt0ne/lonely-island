﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ObjectClasses;

namespace MainProject
{
    public class StoryCard : MonoBehaviour

    {
        [HideInInspector]
        public Transform Card;
        [HideInInspector]
        public NewChar Character;
        public Sprite img;
        public string nameStory;

        int i = 0;
        public IEnumerator OpenCard()
        {
   
            while (Card.rotation.y != 0)
            {
                Card.rotation = Quaternion.RotateTowards(Card.rotation, Quaternion.Euler(0, 0, 0), 7);
                i++;
                if (i == 5000) break;
                yield return new WaitForEndOfFrame();
            }
            Card.GetComponent<DragCard>().enabled = true;
            NewStory();
        }


        public virtual void NewStory() { }
        public virtual void SelectLeft() { Card.GetComponent<HookStory>().OpenNextCard(); }
        public virtual void SelectRight() { Card.GetComponent<HookStory>().OpenNextCard(); }

        public void Start()
        {
            backgraund(img);
        }

        public void Story(string text)
        {
            Text NewText = Scene.Instance.StoryText; ;
            if (NewText) NewText.text = text;
        }

        public void TextLeft(string text)
        {
            for (int i = 0; i < Card.childCount; i++)
            {
                if (Card.GetChild(i).tag == "TextLeft") Card.GetChild(i).GetComponent<Text>().text = text;
            }
        }
        public void TextRight(string text)
        {
            for (int i = 0; i < Card.childCount; i++)
            {
                if (Card.GetChild(i).tag == "TextRight") Card.GetChild(i).GetComponent<Text>().text = text;
            }
        }
        public void backgraund(Sprite img)
        {
            for (int i = 0; i < Card.childCount; i++)
            {
                if (Card.GetChild(i).tag == "FaceCard" && img) Card.GetChild(i).GetComponent<Image>().sprite = img;
            }
        }

        //Получение данных о персонаже который делает ход
        public IEnumerator PortraitAlpha()
        {

            Character = Scene.Instance.CharacterStep.newChar;

            Transform CharSlot = Card.GetComponent<HookStory>().CharSlot;
            CharSlot.GetComponent<Image>().sprite = Character.Portrait;

            while (CharSlot.GetComponent<CanvasGroup>().alpha != 1)
            {
                CharSlot.GetComponent<CanvasGroup>().alpha += 0.1f;
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
