﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainProject
{

    public class MoveCard : MonoBehaviour
    {
        Vector2 newPos;
        float speedMove = 0.1f;
        float speedAlpha = 0.01f;
        bool visable = true;
        public bool Gradient;
        public bool Visable
        { set { visable = value; Gradient = true; } }
        public float SpeedMove
        { set { if (value > 0) speedMove = value; } }
        public float SpeedAlpha
        { set { if (value > 0) speedAlpha = value; } }
        public Vector2 NewPos
        { set { newPos = value; } }



        void Update()
        {
            //плавное появление или затухание
            if (Gradient)
            {
                if (GetComponent<CanvasGroup>() && !visable) GetComponent<CanvasGroup>().alpha += speedAlpha;
                if (GetComponent<CanvasGroup>() && visable) GetComponent<CanvasGroup>().alpha -= speedAlpha;
            }

            if (Vector2.Distance(transform.localPosition, newPos) > 0.5f)
            {
                transform.localPosition = Vector2.Lerp(transform.localPosition, newPos, speedMove);
            }
            else
            {
                transform.localPosition = newPos;
                if (!Gradient) GetComponent<MoveCard>().enabled = false;
                else
                {
                   if (GetComponent<CanvasGroup>().alpha == 1 && !visable) GetComponent<MoveCard>().enabled = false;
                    if (GetComponent<CanvasGroup>().alpha == 0 && visable) GetComponent<MoveCard>().enabled = false;
                }


                if (gameObject.tag == "StartCard") { GetComponent<HookStory>().OpenCard(); GetComponent<DragCard>().enabled = true; }
            }
        }

        public void NewPosition(Vector2 position, float speed)
        {
            NewPos = position;
            speedMove = speed;
            GetComponent<MoveCard>().enabled = true;

        }

    }
}
