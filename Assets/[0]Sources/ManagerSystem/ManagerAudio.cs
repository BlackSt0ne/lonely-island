﻿using Extension;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainProject
{
    public class ManagerAudio : Singleton<ManagerAudio>
    {
        public Transform SoundPlayer;
        Transform SoundGO;
        AudioSource SoundSource;

        private void Awake()
        {

        }

        public void Play(AudioClip clip,float value)
        {

            if (clip)
            {
           
                SoundGO = Instantiate(SoundPlayer);
                SoundSource = SoundGO.GetComponent<AudioSource>();

                DontDestroyOnLoad(SoundGO);

                if (!SoundGO.gameObject.activeSelf)
                    SoundGO.gameObject.SetActive(true);


                SoundSource.clip = clip;
                SoundSource.volume = value;

                SoundSource.Play();
            }

        }

    }


}
