﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector.Demos;

public class Sc_Play : MonoBehaviour 
{
	public int state;
	//public float movT,movT0,movT1;
	//public Vector3 movV0,movA;
	public float chanceSum;// суммарный шанс для квест-карт
	public Vector3 [] v3;
	public Transform trCardInit,trCardPivot,trCardFront;
	public Transform trFlyLeft,trFlyRight;
	public List <Card> cards=new List<Card>();

	// -----------   pers params  --------------
	public List <string> maleNames=new List<string>();
	public List <string> femaleNames=new List<string>();
	public List <Pers> pers=new List<Pers>();
	public int persCount;
	public Transform trPersInit;
	public Transform [] trPersSlots;
	public InventorySlot [] invSlots;
	public SO_Set cardSet;
	public Card activeCard;
	public Pers activePers;
	public List <EventCardControl> eventControls=new List <EventCardControl>();
	public int questCount;
	//

	public Text tStory;


	public GameObject oCard;
	public GameObject oPers;

	public bool persMovable;// можно ли передвигать перса на карту с квестом;

	public static Sc_Play s;

	// Use this for initialization
	void Awake()
	{
		s=this;
		InitNamesList();
	}

	void Start ()
	{
		chanceSum=0;
		for (int i = 0; i < cardSet.Quests.Count; i++) chanceSum+=cardSet.Quests[i].chance;
		InitSetCards();
	}

	void InitNamesList()
	{
		maleNames.Clear();
		foreach(var _n in GP.NameMale) maleNames.Add(_n);
		femaleNames.Clear();
		foreach(var _n in GP.NameFemale) femaleNames.Add(_n);
	}
	public string GetNewName(bool male_)
	{
		string _s;
		int _r;
		if(male_)
		{
			_r=Random.Range(0,maleNames.Count);
			_s=maleNames[_r];
			maleNames.RemoveAt(_r);
		}
		else
		{
			_r=Random.Range(0,femaleNames.Count);
			_s=femaleNames[_r];
			femaleNames.RemoveAt(_r);
		}
		return _s;

	}

	// Update is called once per frame
	void Update ()
	{
		switch(state)
		{
		case 0:
			state=1000;
			break;
		case 1000:
			break;
		case 2000:
			break;
		case 3000:
			break;
		case 4000:
			break;
		case 5000:
			break;
		}

	}

//	void InitCards()
//	{
//		float _t=Time.time+1;
//		for (int i = 0; i < 10; i++)
//		{
//			GameObject _g=Instantiate(oCard,trCardInit) as GameObject;
//			_g.transform.localPosition=Vector3.zero;
//			_g.transform.SetParent(trCardPivot);
//			Card _c=_g.GetComponent<Card>();
//			_c.Init(_t+P.timeInitCardFlyStart*i);
//			cards.Add(_c);
//		}
//		cards[cards.Count-1].Activate(1);
//	}
	void InitSetCards()
	{
		float _t=Time.time+1;
		int _n=-1;
		for (int i = 0; i < cardSet.Intro.Count; i++)//карты интро
		{
			GameObject _g=Instantiate(oCard,trCardInit) as GameObject;
			_g.transform.localPosition=Vector3.zero;
			_g.transform.SetParent(trCardPivot);
			Card _c=_g.GetComponent<Card>();
			if(i>0)_c.Init(cardSet.Intro[i],_t+GP.timeInitCardFlyStart*_n++);
			cards.Add(_c);
		}
		// карты персов
		for (int i = 0; i <5; i++)// одна лишняя для подкладки с рубашкой
		{
			GameObject _g=Instantiate(oCard,trCardInit) as GameObject;
			_g.transform.localPosition=Vector3.zero;
			_g.transform.SetParent(trCardPivot);
			Card _c=_g.GetComponent<Card>();
			int _r=Random.Range(0,cardSet.Pers.Count);
			_c.Init(cardSet.Pers[_r],_t+GP.timeInitCardFlyStart*_n++);
			cards.Add(_c);
		}
		// карта квеста 
		//MakeQwestCard();
			//cards[0].Activate(1);
		cards[0].Init(cardSet.Intro[0],_t+GP.timeInitCardFlyStart*_n++);// дождаться всей колоды до открытия
		activeCard=cards[0];//
		activeCard.Activate(1);
		activeCard.transform.SetParent(trCardFront);

		// Создание списка карт периодических событий
		eventControls.Clear();
		foreach(var _c in cardSet.Events) eventControls.Add(new EventCardControl(_c));


	}
	void MakeQuestCard(int ind_)
	{
		GameObject _g=Instantiate(oCard,trCardPivot) as GameObject;
		_g.transform.localPosition=Vector3.zero;
		_g.transform.SetParent(trCardPivot);
		Card _c=_g.GetComponent<Card>();
		float _r=Random.Range(0f,chanceSum);
		float _s=0;
		for (int i = 0; i < cardSet.Quests.Count; i++) 
		{
			_s+=cardSet.Quests[i].chance;
			if(_s>_r)
			{
				_c.Init(cardSet.Quests[i]);
				cards.Insert(ind_,_c);
				return;//break;
			}
		}
	}
	void MakeEventCard(int ind_,int num_)
	{
		GameObject _g=Instantiate(oCard,trCardPivot) as GameObject;
		_g.transform.localPosition=Vector3.zero;
		_g.transform.SetParent(trCardPivot);
		Card _c=_g.GetComponent<Card>();
		_c.Init(cardSet.Events[num_]);
		cards.Insert(ind_,_c);
	}

	void MakeDieCard(int ind_,Pers pers_)
	{
		GameObject _g=Instantiate(oCard,trCardPivot) as GameObject;
		_g.transform.localPosition=Vector3.zero;
		_g.transform.SetParent(trCardPivot);
		Card _c=_g.GetComponent<Card>();
		_c.Init(cardSet.Die, pers_);
		cards.Insert(ind_,_c);
	}
	void MakeEndCard(int ind_)
	{
		GameObject _g=Instantiate(oCard,trCardPivot) as GameObject;
		_g.transform.localPosition=Vector3.zero;
		_g.transform.SetParent(trCardPivot);
		Card _c=_g.GetComponent<Card>();
		_c.Init(cardSet.End);
		cards.Insert(ind_,_c);
	}

	public void ActivateNext()
	{
		if((cards.Count)<3)
		{
			bool _d=false;
			if(pers.Count>0)
			{
				for (int i = 0; i < pers.Count; i++) 
				{
					if(pers[i].trigDie)
					{
						MakeDieCard(1,pers[i]);
						pers.RemoveAt(i);
						_d=true;
						break;
					}
				}
				if(!_d)
				{
					bool _ev=false;
					for(int i=0;i<eventControls.Count;i++)
					{
						if(eventControls[i].Ready(questCount))
						{
							_ev=true;
							MakeEventCard(1,i);
							break;
						}
					}
					if(!_ev) MakeQuestCard(1);
				}
			}
			else MakeEndCard(1);
		}
		activeCard=cards[1];//вторая карта
		activeCard.Activate(1);
			//activeCard.transform.SetParent(trCardFront);
			//activeCard=null;

		cards.RemoveAt(0);
		persMovable=false;
	}

	public void SetStoryText(string story_)
	{
		tStory.text=story_;
	}

	public void AddPers(SO_CardPers soCardPers_,string  persName_)
	{
		GameObject _g=Instantiate(oPers) as GameObject;
		Pers _p=_g.GetComponent<Pers>();
		_p.Init(persName_,soCardPers_);

		pers.Add(_p);
		_p.transform.SetParent(trPersInit,false);
		_p.transform.localPosition=Vector3.zero;
		//p_.transform.position=trPersInit.position;
		_p.transform.SetParent(trPersSlots[persCount++]);//,false);
	}

	public bool AddToInventory(SO_Card soCard_)//CardBase c_)
	{
		bool _ok=false;
		for (int i = 0; i < 4; i++)
		{
			if(invSlots[i].IsEmpty())
			{
				invSlots[i].Add(soCard_);
				_ok=true;
				break;
			}
		}
		return _ok;
	}

	public GameObject GetPersPrefab()
	{
		return oPers;
	}

	public void PersMovable(bool b_)
	{
		persMovable=b_;
	}
	public void PersTakeQuest(Pers p_)
	{
		activePers=p_;
		persMovable=false;
		activeCard.PlayQuest(p_.transform.parent);
	}

	public bool IsPersMovable()
	{
		return persMovable;
	}
//	public SO_CardQuest PlayQuestCard(Transform slot_)
//	{
//		activeCard.PlayQuest(slot_);
//		return (SO_CardQuest) activeCard.soCard;
//	}

	public void Turn() // передать персам о ходе
	{
		foreach(var _pers in pers) _pers.Turn();
			
	}

	public void IncreaseQuestCounter()
	{
		questCount++;
	}

	public void ApplyQuestToPers(Quest q_)// передать персам выполнение квеста/эффекта
	{
		if(q_.group)
			foreach(var _pers in pers) _pers.AddQuest(q_);
		else
			activePers.AddQuest(q_);
	}

	public Transform GetActiveCardPersPivot()
	{
		return activeCard.GetPersPivot();
	}


}
