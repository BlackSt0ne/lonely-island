﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="NewCardInfo",menuName="Cards/new Info Card")]

public class SO_Card : ScriptableObject
{
	public CARD_TYPE type;// тип карты (поведение карты)
    public string cardName;
    public float chance = 1; // вес, вероятность выпадения
    [PreviewField(70, ObjectFieldAlignment.Center)]
	public Sprite spr;
	public AudioClip clip;
    public string sLeft, sRight;
    [Multiline(6)]
    public string sStory;

    public SO_Card()
	{
		SetCardType(CARD_TYPE.BASE);
	}

	public CARD_TYPE GetCardType()
	{
		return type;
	}
	public  void SetCardType(CARD_TYPE t_)
	{
		type=t_;
	}
}
