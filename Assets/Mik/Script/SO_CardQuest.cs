﻿using System.Collections;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="NewCardQuest",menuName="Cards/new Quest Card")]

public class SO_CardQuest : SO_Card
{

	public bool hardQuest;// обязательный квест, нельзя отбрасывать карту в сторону



	public List <Quest> quests;
    //public List <SO_Effect> effects;

    //public float effectChanceProcent;
    //public bool group;
    //public string effectStory;
    //public Sprite sprEffect;
    //public List <Quest> chanceQuests;
    [BoxGroup("Effects")]
    public bool bipolarEffect;

    [BoxGroup("Effects")]
    public float strength, skill, intel;

    [BoxGroup("Effects")]
    public List <SO_Effect> positiveEffects, negativeEffects;

	//public List <QuestEffect> effects;

	public SO_CardQuest()
	{
		SetCardType(CARD_TYPE.QUEST);
	}



    //добавление эффектов через кнопку в инспекторе
    [BoxGroup("Effects")]
    public string EffectName;

    [BoxGroup("Effects")]
    [Button("AddPositiveEffects")]
    void AddPositiveEffects()
    {
        var asset = ScriptableObjectUtility.CreateAsset<SO_Effect>(EffectName) as SO_Effect;
        positiveEffects.Add(asset);
    }
    [BoxGroup("Effects")]
    [Button("AddNegativeEffects")]
    void AddNegativeEffects()
    {
        var asset = ScriptableObjectUtility.CreateAsset<SO_Effect>(EffectName) as SO_Effect;
        negativeEffects.Add(asset);
    }

}
