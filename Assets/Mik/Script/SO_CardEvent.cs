﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName="NewCardEvent",menuName="Cards/new Event Card")]


public class SO_CardEvent : SO_CardQuest
{
	public int start;
	public int period;
	public SO_CardEvent()
	{
		type=CARD_TYPE.EVENT;
	}

}

public class EventCardControl
{
	SO_CardEvent card;
	int period;
//	int start;
	bool done;

	public EventCardControl(SO_CardEvent card_)
	{
		card=card_;
	}

	public bool Ready(int c_)
	{
		int _p=c_/card.period;
		int _s=c_-_p*card.period;

		if(_p!=period)
		{
			period=_p;
			done=false;
		}

		if(_s==card.start)
		{
			if(done) return false;
			else
			{
				done=true;
				return true;
			}
		}
		else return false;
	}

					
	
}
