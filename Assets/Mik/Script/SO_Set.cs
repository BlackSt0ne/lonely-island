﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName="NewSet",menuName="new Set")]

public class SO_Set : ScriptableObject
{
    [TabGroup("Intro")]
    public List<SO_Card> Intro;
    [TabGroup("Pers")]
    public List<SO_Card> Pers;
    [TabGroup("Quests")]
	public List<SO_Card> Quests;
	[TabGroup("FreeEvent")]
	public List <SO_CardEvent> Events;

    [TabGroup("HardEvent")]
    public SO_Card Die;
    [TabGroup("HardEvent")]
    public SO_Card End;



    //группировка и расширение SO через кнопку в инспекторе

    [TabGroup("Intro")]
    public string IntroName;

    [TabGroup("Pers")]
    public string PersName;

    [TabGroup("Quests")]
    public string QuestsName;

    [TabGroup("Intro")]
    [Button("AddIntro")]
    void AddIntro()
    {
        var asset = ScriptableObjectUtility.CreateAsset<SO_Card>(IntroName) as SO_Card;
        Intro.Add (asset);
    }

    [TabGroup("Pers")]
    [Button("AddPers")]
    void AddPers()
    {
        var asset = ScriptableObjectUtility.CreateAsset<SO_CardPers>(PersName) as SO_Card;
        Pers.Add(asset);
    }

    [TabGroup("Quests")]
    [Button("AddQuests")]
    void AddQuests()
    {
        var asset = ScriptableObjectUtility.CreateAsset<SO_CardQuest>(QuestsName) as SO_Card;
        Quests.Add(asset);
    }

}
