﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName="NewCardPers",menuName="Cards/new Pers Card")]

public class SO_CardPers : SO_Card
{
	public bool male;// gender
                     //public string persName;
    [PreviewField(70, ObjectFieldAlignment.Center)]
    public Sprite face;
	public float strength;
	public float scill;
	public float intellect;

	public SO_CardPers()
	{
		SetCardType(CARD_TYPE.PERS);
	}


}
