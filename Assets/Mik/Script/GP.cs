﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// класс констант и параметров в игре Game Params
public class GP 
{
	// параметры при сдаче колоды
	public static float timeInitCardFly=1;//время полета карты
	public static float timeInitCardFlyStart=0.2f;//интервал при сдаче карт
	public static float timeInitCardTurn=0.5f;// время переворота карты

	public static float cardAngle=8f;// угол наклона для карты, когда она срабатывает

	public static string[] NameMale = { "Малколм", "Эрон", "Роналд", "Джон", "Чарлз", "Сесил", "Энтони", "Кристофер", "Джозеф", "Гарри" };
	public static string[] NameFemale = { "Бернис", "Джоан", "Вивиан", "Сьюзан", "Мэрилинн", "Беверли", "Мартина", "Клер", "Имоджен", "Эмис" };

	public static float persToCardDistance=130;// при переносе перса на карту с квестом
	public static float tCardToPersDelay=0.3f;// задержка при переносе перса на карту с квестом
	public static float tCardToPersFly=0.6f;// перелет при переносе перса на карту с квестом
	public static float tPersBarChange=1f;// время в течении которого изменяется размер и цвет бара параметра перса

	public static float tEffectCardDelay=0.6f;// задержка перед переворотом карты с эффектом
	public static float tEffectCardTurn=0.5f;// время переворота карты с эффектом

	public static float [] propMax=
	{
		100,	//MIND,
		100,	//		FOOD,
		100,	//		WATER,
		100,	//		HP,
		10,	//		STRENGTH,
		10,	//		SKILL,
		10,	//		INTEL,
		10	//		LAST
	};

}
