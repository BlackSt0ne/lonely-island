﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour 
{
	public bool local;
	public float movT,movT0,movT1;
	public Vector3 movV0,movA;
	public Vector3 p0,p1;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Time.time>movT0)
		{
			float _t=Time.time-movT0;
			if(local)transform.localPosition=p0+(movV0+movA*_t)*_t;
			else transform.position=p0+(movV0+movA*_t)*_t;//0.5Time.deltaTime*(movV0+movA*(Time.time-movT0));
			if(_t>movT)
			{
				if(local) transform.localPosition=p1;
				else transform.position=p1;
				Destroy(this);
			}
		}

	}

	public void Init(bool local_,Vector3 p1_,float t_)
	{
		local=local_;
		movT0=Time.time;
		p1=p1_;
		movT=t_;
		movT1=movT0+movT;
		if(local) p0=transform.localPosition;
		else p0=transform.position;

		movA=-(p1-p0)*2/(movT*movT);
		movV0=-movA*movT;
		movA*=0.5f;
	}

	public void Init(bool local_,Vector3 p1_,float t_,float t0_)
	{
		local=local_;
		movT0=t0_;
		p1=p1_;
		movT=t_;
		movT1=movT0+movT;
		if(local) p0=transform.localPosition;
		else p0=transform.position;

		movA=-(p1-p0)*2/(movT*movT);
		movV0=-movA*movT;
		movA*=0.5f;
	}

}
