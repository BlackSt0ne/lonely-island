﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NewEffect", menuName = "new Effect")]
public class SO_Effect :  ScriptableObject
{
	public float effectChance;

	public string storyEffect;
	public Sprite spriteEffect;
	public List <Quest> quests;
//    public PROPERTY prop;// на какое качество влияет
//    public float value; // значение, которое добавляется или вычитается
//    public int turns = 1; // сколько ходов применяется квест или эффект
//    public bool group;  // применить ко всем персам

}


//    public SO_Effect()
//    {
//        turns = 1;
//    }
//    public SO_Effect(SO_Effect q_)// копирующий конструктор
//    {
//        prop = q_.prop;
//        value = q_.value;
//        turns = q_.turns;
//        group = q_.group;
//
//    }
//    public SO_Effect(Quest q_)// преобразующий конструктор
//    {
//        prop = q_.prop;
//        value = q_.value;
//        turns = q_.turns;
//        group = q_.group;
//    }

[System.Serializable]
public class Quest
{
    public PROPERTY prop;// на какое качество влияет
    public float value; // значение, которое добавляется или вычитается
    public int turns = 1; // сколько ходов применяется квест или эффект
    public bool group;  // применить ко всем персам

    public Quest()
    {
        turns = 1;
    }
    public Quest(Quest q_)// копирующий конструктор
    {
        prop = q_.prop;
        value = q_.value;
        turns = q_.turns;
        group = q_.group;
    }

    public Quest(SO_Effect e_,int i_)// преобразующий конструктор
    {

        prop = e_.quests[i_].prop;
		value = e_.quests[i_].value;
		turns = e_.quests[i_].turns;
		group = e_.quests[i_].group;
    }

}



