﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using Sirenix.Serialization;
using Sirenix.OdinInspector.Demos;

public class Card : MonoBehaviour , IBeginDragHandler, IDragHandler, IEndDragHandler
{
//	public float debF;
//	public Vector3 debV;
	public string cardName;
	public int state;
	public int active; // on top koloda 1-turn 2-drag 3- fly to right/left 4- play with pers
//	public int keep; // 0- ... 1- хранится в инвентаре
	public Sprite spr;
	public string sStory;
	public string persName;// имя перса если карта персонажа
	public Text tTitle;
	public Image im;
	public float movT,movT0,movT1;
	public Vector3 movV0,movA;
	public AudioClip cTake,cFly;
	//public Vector3 [] v3;
	public Transform [] tr;
	public Transform trPersPivot;
	public Move move;
	public Vector3 offset, startPos;
	//CardBase cardBase;
	public SO_Card soCard;
	SO_CardPers soCardPers;
	SO_CardQuest soCardQuest;
	SO_CardEvent soCardEvent;
	public bool hardQuest;
	public bool playQuestEffect;// будет ли проявляться эффект
	float t1,t,t0;
	float u1,u,u0;
	Pers pers;
	SO_Effect effect; // сработавший эффесе

	Transform trFlyTo;// куда лететь квест карте на игрока

	// Use this for initialization

	void Awake()
	{
		//cardBase=GetComponent<CardBase>();
	}
	void Start () 
	{
		StartVirt();
	}
	public virtual void StartVirt()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
		//debF=tr[0].localEulerAngles.y;
		switch(state)
		{
		case 0:
			if(!move) state=1000;
			break;
		case 1000://
			if(active==1)
			{
				transform.SetParent(Sc_Play.s.trCardFront);
				movT0=Time.time;
				movT1=Time.time+GP.timeInitCardTurn;
				movT=1/GP.timeInitCardTurn;
				state=1100;
			}
			break;
		case 1100://переворот
			if(Time.time<movT1)
			{
				tr[0].localEulerAngles=new Vector3(0,180+180*(Time.time-movT0)*movT,0);
				if(tr[0].localEulerAngles.y>270) 
				{
					im.sprite=soCard.spr;
					state=1200;
				}
			}
			else
			{
				tr[0].localEulerAngles=new Vector3(0,0,0);
				active=2;
				state=2000;
			}
			break;
		case 1200:
			if(Time.time<movT1)
			{
				tr[0].localEulerAngles=new Vector3(0,180+180*(Time.time-movT0)*movT,0);
			}
			else
			{
				tr[0].localEulerAngles=new Vector3(0,0,0);
				active=2;
				state=2000;
			}
			break;
		case 2000:// переворот окончен
			Sc_Play.s.SetStoryText(sStory);
			if(soCard.clip) Sound.s.PlaySound(soCard.clip);
			if(soCard.GetCardType()==CARD_TYPE.QUEST)
			{
				Sc_Play.s.PersMovable(true);
			}

			state=2010;
			break;
		case 2010:// ожидание
			break;
		case 2100:// отброс влево
			if(!move)
			{
//				if(keep==1) state=2400;// сохранить в инвентаре
//				else 
					Destroy(gameObject);
			}
			break;
		case 2200://отброс вправо
			if(!move)
			{
//				if(keep==1) state=2400;// сохранить в инвентаре
//				else 
					Destroy(gameObject);
			}
			break;
		case 2300: // возврат в исходное положение
			transform.rotation = Quaternion.Euler(0, 0, -0.1f * transform.localPosition.x);
			if(!move)
			{
				transform.rotation = Quaternion.Euler(0, 0,0);
				state=2000;
			}
			break;
		case 2400: // храниться в инвентаре
			break;
		case 3000: // карта квеста падает на перса
			t0=Time.time+GP.tCardToPersDelay;
			state=3010;
			break;
		case 3010: // карта квеста падает на перса
			//Sc_Play.s.ActivateNext();
			//Vector3 transform.SetParent(trFlyTo);
			if(t0<=Time.time)
			{
				t0=Time.time+GP.tCardToPersFly;
				t1=1/GP.tCardToPersFly;
				move=gameObject.AddComponent<Move>();
	//			move.Init(true,Vector3.zero,GP.tCardToPersFly);
				move.Init(false,trFlyTo.TransformPoint(0,0,0),GP.tCardToPersFly);
				Implement();
				state=3100;
			}

			break;
		case 3100: // карта квеста падает на перса
			float _s=(t0-Time.time)*t1;
			im.color=new Color (1,1,1,_s);
			transform.localScale=new Vector3(_s,_s,1);
			if(!move) 
			{
				Sc_Play.s.ActivateNext();
				Destroy(gameObject);
			}

			break;

		case 4000:// играем эффект к квесту
			t0=Time.time+GP.tEffectCardDelay;
			state=4100;

			break;
		case 4100:
			if(t0<=Time.time)
			{
				Sc_Play.s.SetStoryText(effect.storyEffect);
				t0=Time.time+GP.tEffectCardTurn;
				u=0;
				u1=180/GP.tEffectCardTurn;
				state=4200;
			}
			break;
		case 4200:
			u+=u1*Time.deltaTime;
			if(u>=90)
			{
				u-=180;
				im.sprite=effect.spriteEffect;//soCardQuest.sprEffect;
				state=4300;
			}
			else
			{
				transform.localEulerAngles=new Vector3(0,u,0);
			}
			break;
		case 4300:
			u+=u1*Time.deltaTime;
			if(u>=0)
			{
				transform.localEulerAngles=Vector3.zero;
				state=3000;
			}
			else
			{
				transform.localEulerAngles=new Vector3(0,u,0);
			}
			break;
		}
//		UpdateVirt();
	}
//	public virtual void UpdateVirt()
//	{
//	}

	public void Init(SO_Card soCard_,float t_)
	{
		soCard=soCard_;
		cardName=soCard.cardName;
		sStory=soCard.sStory;

		if(soCard.GetCardType()==CARD_TYPE.PERS)
		{
			soCardPers=(SO_CardPers)soCard;
			persName=Sc_Play.s.GetNewName(soCardPers.male);
			sStory=persName+". "+soCardPers.sStory;
		}
		else if(soCard.GetCardType()==CARD_TYPE.QUEST)
		{
			soCardQuest=(SO_CardQuest)soCard;
			hardQuest=soCardQuest.hardQuest;
		}
		else if(soCard.GetCardType()==CARD_TYPE.EVENT)
		{
			soCardEvent=(SO_CardEvent)soCard;
		}
		movT0=t_;
		move=gameObject.AddComponent<Move>();
		move.Init(true,Vector3.zero,GP.timeInitCardFly,movT0);

	}
	public void Init(SO_Card soCard_)
	{
		soCard=soCard_;
		cardName=soCard.cardName;
		sStory=soCard.sStory;

		if(soCard.GetCardType()==CARD_TYPE.PERS)
		{
			soCardPers=(SO_CardPers)soCard;
			persName=Sc_Play.s.GetNewName(soCardPers.male);
			sStory=persName+". "+soCardPers.sStory;
		}
		else if(soCard.GetCardType()==CARD_TYPE.QUEST)
		{
			soCardQuest=(SO_CardQuest)soCard;
			hardQuest=soCardQuest.hardQuest;
		}
		else if(soCard.GetCardType()==CARD_TYPE.EVENT)
		{
			soCardEvent=(SO_CardEvent)soCard;
		}

	}
	public void Init(SO_Card soCard_,Pers pers_)//
	{
		pers=pers_;
		soCard=soCard_;
		cardName=soCard.cardName;
		sStory=soCard.sStory.Replace("*",pers_.persName);
		soCardQuest=(SO_CardQuest)soCard;
//
//		if(soCard.GetCardType()==CARD_TYPE.PERS)
//		{
//			soCardPers=(SO_CardPers)soCard;
//			persName=Sc_Play.s.GetNewName(soCardPers.male);
//			sStory=persName+". "+soCardPers.sStory;
//		}
//		else if(soCard.GetCardType()==CARD_TYPE.QUEST)
//		{
//			soCardQuest=(SO_CardQuest)soCard;
//		}
//		//		movT0=t_;
//		//		move=gameObject.AddComponent<Move>();
//		//		move.Init(true,Vector3.zero,GP.timeInitCardFly,movT0);
//
	}

	public void Activate(int n_)
	{
		active=n_;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		if(active==2 && !hardQuest)
		{
			Sound.s.PlaySound(cTake);
			offset = transform.localPosition - Camera.main.ScreenToWorldPoint(eventData.position);
			startPos = transform.position;
			// localPosition , position
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (active==2 && !hardQuest)
		{
			Vector3 _pos = Camera.main.ScreenToWorldPoint(eventData.position)+ offset;
			transform.localPosition = new Vector3(_pos.x,_pos.y,0);//_pos + offset;

			transform.rotation = Quaternion.Euler(0, 0, -0.1f * transform.localPosition.x);
			float _u=transform.localEulerAngles.z;
			if(_u>180) _u-=360;
			if(_u>1) tTitle.text=soCard.sLeft;
			else if(_u>=-1) tTitle.text="";
			else if(_u<-1) tTitle.text=soCard.sRight;
				
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if(active==2 && !hardQuest)
		{
			float _u=transform.localEulerAngles.z;
			if(_u>180) _u-=360;
			if(_u>GP.cardAngle)
			{
				move=gameObject.AddComponent<Move>();
				move.Init(false,Sc_Play.s.trFlyLeft.position,GP.timeInitCardFly);
				state =2100;
				active=3;
				Sc_Play.s.ActivateNext();
				Sound.s.PlaySound(cFly);
				//cardBase.Left();
				Implement();
			}
			else if(_u<-GP.cardAngle)
			{
				move=gameObject.AddComponent<Move>();
				move.Init(false,Sc_Play.s.trFlyRight.position,GP.timeInitCardFly);
				state =2200;
				active=3;
				Sc_Play.s.ActivateNext();
				Sound.s.PlaySound(cFly);
				//cardBase.Right();
				Implement();
			}
			else //if(_u!=0)
			{
				move=gameObject.AddComponent<Move>();
				move.Init(true,Vector3.zero,0.3f);
				tTitle.text="";
				state =2300;
			}
		}
	}
//	public string GetStoryText()
//	{
//		return soCard.sStory;
//	}
//	public void SetStoryText(string c_)
//	{
//		soCard.sStory=c_;
//	}

//	public void SetKeep(int i_)
//	{
//		keep=i_;
//	}

	public void PlayQuest(Transform trFlyTo_)
	{
		active=4;
		trFlyTo=trFlyTo_;
		effect=null;
		if(soCardQuest.bipolarEffect)
		{
			float _p0,_p1;
			if(soCardQuest.strength>0)_p0=0.5f*Sc_Play.s.activePers.prop[(int)PROPERTY.STRENGTH]/soCardQuest.strength;
			else _p0=0;

			if(soCardQuest.skill>0) _p1=0.5f*Sc_Play.s.activePers.prop[(int)PROPERTY.SKILL]/soCardQuest.skill;
			else _p1=0;

			if(_p1>_p0) _p0=_p1;

			if(soCardQuest.intel>0) _p1=0.5f*Sc_Play.s.activePers.prop[(int)PROPERTY.INTEL]/soCardQuest.intel;
			else _p1=0;

			if(_p1>_p0) _p0=_p1;

			if(_p0>Random.value) SelectEffectBP(soCardQuest.positiveEffects);
			else SelectEffectBP(soCardQuest.negativeEffects);
		}
		else
		{
			SelectEffectSimple();
		}
		playQuestEffect=effect!=null;
		if(playQuestEffect)	state=4000;
		else state=3000;
	}

	public void SelectEffectBP(List<SO_Effect> ef_) // выбор биполярного эффекта
	{
		float _sum=0;
		for (int i = 0; i < ef_.Count; i++) _sum+=ef_[i].effectChance;
		float _r=Random.Range(0f,_sum);
		float _s=0;
		for (int i = 0; i < ef_.Count; i++) 
		{
			_s+=ef_[i].effectChance;
			if(_s>=_r)
			{
				effect=ef_[i];
				return;//break;
			}
		}
	}

	public void SelectEffectSimple() // выбор биполярного эффекта
	{
		List <SO_Effect> _ef=new List<SO_Effect>();
		foreach(var _e in soCardQuest.positiveEffects) _ef.Add(_e);
		foreach(var _e in soCardQuest.negativeEffects) _ef.Add(_e);
		//float _sum=101;
		float _r=Random.Range(1,101);
		float _s=0;
		for (int i = 0; i < _ef.Count; i++) 
		{
			_s+=_ef[i].effectChance;
			if(_s>=_r)
			{
				effect=_ef[i];
				return;//break;
			}
		}
	}

	void Implement()
	{
		switch(soCard.GetCardType())
		{
		case CARD_TYPE.PERS:
			Sc_Play.s.AddPers(soCardPers,persName);
			break;
		case CARD_TYPE.QUEST:
			Sc_Play.s.IncreaseQuestCounter();
			if(state==2100) // fly left
			{
				Sc_Play.s.AddToInventory(soCard);
			}
			else if(state==3010)// pers take card
			{
				foreach(var _q in soCardQuest.quests) Sc_Play.s.ApplyQuestToPers(_q);
				//foreach(var _e in soCardQuest.effects) Sc_Play.s.ApplyQuestToPers(new Quest(_e));
				if(playQuestEffect)
				{
					if(effect)
					{
						for(int i=0; i<effect.quests.Count; i++)
						{
							Sc_Play.s.ApplyQuestToPers(new Quest(effect,i));
						}
					}
					//foreach(var _q in soCardQuest.chanceQuests) Sc_Play.s.ApplyQuestToPers(_q);
					//foreach(var _e in soCardQuest.chanceEffects) Sc_Play.s.ApplyQuestToPers(new Quest(_e));
				}
			}
			Sc_Play.s.Turn();
			break;
		case CARD_TYPE.EVENT:
			foreach(var _q in soCardEvent.quests) Sc_Play.s.ApplyQuestToPers(_q);
			Sc_Play.s.Turn();
			break;
		case CARD_TYPE.DIE:
			pers.Die();
			foreach(var _q in soCardQuest.quests) Sc_Play.s.ApplyQuestToPers(_q);
			Sc_Play.s.Turn();
			break;
		case CARD_TYPE.END:
			SceneManager.LoadScene("Sc_Play");
			break;

		}

	}

	public Transform GetPersPivot()
	{
		return trPersPivot;
	}
}
