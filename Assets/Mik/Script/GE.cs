﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//  Файл со всеми ЕНУМ в игре

public enum CARD_TYPE {
	BASE, // остальные карты ( простые)
	PERS, // карты персов
	QUEST, // карты квестов
	DIE,	// карта смерти перса
	END,	// все сдохли
	EVENT,	// карта периодических событий
	LAST
}

public enum PROPERTY
{
	MIND,
	FOOD,
	WATER,
	HP,
	STRENGTH,
	SKILL,
	INTEL,
	LAST
}

//public class GE : MonoBehaviour {
//
//	// Use this for initialization
//	void Start () {
//		
//	}
//	
//	// Update is called once per frame
//	void Update () {
//		
//	}
//}
