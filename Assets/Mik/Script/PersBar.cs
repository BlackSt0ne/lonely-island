﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersBar : MonoBehaviour
{
	
	public int state;
	public Image im;
	public float value=1;
	public float v1,vel,t1;

	// Use this for initialization
	void Start () 
	{
		im=GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(state==1)
		{
			if(t1>Time.time)
			{
				value+=vel*Time.deltaTime;
			}
			else
			{
				value=v1;
				state=0;
			}
			im.color=new Color(1,value,value,1);
			im.fillAmount=value;	
		}
	}

	public void NewVal(float v_)
	{
		v1=v_;
		t1=Time.time+GP.tPersBarChange;
		vel=(v1-value)/GP.tPersBarChange;
		state=1;
	}
}
