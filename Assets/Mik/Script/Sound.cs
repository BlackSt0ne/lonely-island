﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
	public AudioSource aMusic;
	public AudioSource aSound;
	public static Sound s;

	// Use this for initialization
	void Awake () 
	{
		s=this;
	}

	void Start () 
	{
		aSound=gameObject.AddComponent<AudioSource>() as AudioSource;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void PlaySound(AudioClip c_)
	{
		aSound.PlayOneShot(c_);

	}

}
