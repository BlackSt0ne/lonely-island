﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Pers : MonoBehaviour , IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public string persName;

	public float t;
	public bool trigMove;
	public bool trigDie;

	public GameObject [] tools;
	public Transform trFace;
	//public Transform []tr; 
	public Image [] iFace;
//	public Image iBack;
//	public Image iMind;
//	public Image iFood;
//	public Image iWater;
	public PersBar iBack;
	public PersBar iMind;
	public PersBar iFood;
	public PersBar iWater;
	public Text tName;
	public SO_CardPers soCardPers;
	public Vector3 offset;
	public Transform parent;
	public Sprite sDie;
	SO_CardQuest soCardQuest;
	public List <Quest> quests;

	public float [] prop;

	// Use this for initialization
	void Start ()
	{
		Move _m=gameObject.AddComponent<Move>();
		_m.Init(true,Vector3.zero,t);

		_m=tools[0].AddComponent<Move>();
		_m.Init(true,Vector3.zero,t);
		_m=tools[1].AddComponent<Move>();
		_m.Init(true,Vector3.zero,t);
		_m=tools[2].AddComponent<Move>();
		_m.Init(true,Vector3.zero,t);

		parent=transform.parent;

		prop[(int)PROPERTY.HP]=100;
//		prop[(int)PROPERTY.MIND]=100;
//		prop[(int)PROPERTY.FOOD]=100;
//		prop[(int)PROPERTY.WATER]=100;
		RefreshView();

	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void Init(string name_,SO_CardPers soCardPers_)
	{
		persName=name_;
		tName.text=persName;
		soCardPers=soCardPers_;
		iFace[0].sprite=soCardPers.face;
		iFace[1].sprite=soCardPers.face;
		prop[(int)PROPERTY.INTEL]=soCardPers.intellect;
		prop[(int)PROPERTY.SKILL]=soCardPers.scill;
		prop[(int)PROPERTY.STRENGTH]=soCardPers.strength;

		prop[(int)PROPERTY.FOOD]=(int)Random.Range(30,101);
		prop[(int)PROPERTY.WATER]=(int)Random.Range(30,101);
		prop[(int)PROPERTY.MIND]=(int)Random.Range(30,101);



	}
	public void OnBeginDrag(PointerEventData eventData)
	{
		if(Sc_Play.s.IsPersMovable() && !trigDie)
		{
			trigMove=true;
			trFace.SetParent(Sc_Play.s.trCardFront);
			offset = trFace.localPosition - Camera.main.ScreenToWorldPoint(eventData.position);
			//startPos = transform.position;
			// localPosition , position
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (trigMove)//Sc_Play.s.IsPersMovable())
		{
			
			Vector3 _pos = Camera.main.ScreenToWorldPoint(eventData.position)+ offset;
			trFace.localPosition = new Vector3(_pos.x,_pos.y,0);//_pos + offset;
//			if(trFace.localPosition.magnitude<GP.persToCardDistance)
//			{
//				trFace.SetParent(parent);
//				Sc_Play.s.PersTakeQuest(this);
//				//soCardQuest=Sc_Play.s.PlayQuestCard(parent);
//				//PlayQuest();
//				Move _m=gameObject.AddComponent<Move>();
//				_m.Init(true,Vector3.zero,0.3f);
//			}
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if(trigMove)//Sc_Play.s.IsPersMovable())
		{
			trigMove=false;
			if(trFace.localPosition.magnitude<GP.persToCardDistance)
			{
				Sc_Play.s.PersMovable(false);
				trFace.SetParent(Sc_Play.s.GetActiveCardPersPivot());
				trFace.localPosition=Vector3.zero;
				Sc_Play.s.PersTakeQuest(this);
				//soCardQuest=Sc_Play.s.PlayQuestCard(parent);
				//PlayQuest();
				//Move _m=gameObject.AddComponent<Move>();
				//_m.Init(true,Vector3.zero,0.3f);
				GameObject _g=Instantiate(iFace[0].gameObject,transform) as GameObject;
				trFace=_g.transform;
			}
			else
			{
				{
					trFace.SetParent(transform);
						Move _m=trFace.gameObject.AddComponent<Move>();
					_m.Init(true,Vector3.zero,0.3f);
				}
			}
		}
	}

	public void AddQuest(Quest q_)
	{
		Quest _q=new Quest(q_);
		quests.Add(_q);
	}

	public void Turn() // сделать ход по отработке эффектов
	{
		
//		int n=0;
		if(prop[(int)PROPERTY.FOOD]<=0) prop[(int)PROPERTY.HP]-=5;
		if(prop[(int)PROPERTY.WATER]<=0) prop[(int)PROPERTY.HP]-=5;

		for (int i = quests.Count-1; i >=0; i--) 
		{
			prop[(int)(quests[i].prop)]+=quests[i].value;
			if(prop[(int)(quests[i].prop)]>GP.propMax[(int)(quests[i].prop)])
				prop[(int)(quests[i].prop)]=GP.propMax[(int)(quests[i].prop)];
			if(prop[(int)(quests[i].prop)]<0)
			{
				prop[(int)(quests[i].prop)]=0;
			}
			
			if(--quests[i].turns<=0) quests.RemoveAt(i);
		}
		if(prop[(int)PROPERTY.HP]<=0)
		{
			trigDie=true;
			trFace.gameObject.GetComponent<Image>().sprite=sDie;
		}


//		foreach(var _q in quests)
//		{
//			prop[(int)(_q.prop)]+=_q.value;
//			if(prop[(int)(_q.prop)]>GP.propMax[(int)(_q.prop)])
//				prop[(int)(_q.prop)]=GP.propMax[(int)(_q.prop)];
//			
//			if(--_q.turns<=0) quests.Remove(_q);
//		}
		RefreshView();
	}

	public void RefreshView() // обновить вид перса
	{
//		float _r;
//
//		_r=prop[(int)PROPERTY.MIND]/GP.propMax[(int)PROPERTY.MIND];
//		iMind.color=new Color(1,_r,_r,1);
//		iMind.fillAmount=_r;
		iMind.NewVal(prop[(int)PROPERTY.MIND]/GP.propMax[(int)PROPERTY.MIND]);
//
//		_r=prop[(int)PROPERTY.FOOD]/GP.propMax[(int)PROPERTY.FOOD];
//		iFood.color=new Color(1,_r,_r,1);
//		iFood.fillAmount=_r;
		iFood.NewVal(prop[(int)PROPERTY.FOOD]/GP.propMax[(int)PROPERTY.FOOD]);
//
//		_r=prop[(int)PROPERTY.WATER]/GP.propMax[(int)PROPERTY.WATER];
//		iWater.color=new Color(1,_r,_r,1);
//		iWater.fillAmount=_r;
		iWater.NewVal(prop[(int)PROPERTY.WATER]/GP.propMax[(int)PROPERTY.WATER]);

		//_r=prop[(int)PROPERTY.HP]/GP.propMax[(int)PROPERTY.HP];
		iBack.NewVal(prop[(int)PROPERTY.HP]/GP.propMax[(int)PROPERTY.HP]);
	}

	public void Die()
	{
		Destroy(gameObject);
	}
}
