﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour 
{
	public Image img;
	public SO_Card soCard;

	// Use this for initialization
	void Start () 
	{
		//img=GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public bool IsEmpty()
	{
		return (soCard==null);

	}
	public void Add(SO_Card soCard_)
	{
		soCard=soCard_;
		img.sprite=soCard.spr;
		img.enabled=true;
	}

}
